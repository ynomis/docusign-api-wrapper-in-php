# Summary #

DocuSign doesn't provide full php client library. This project is to create a small package gathering some commonly used features through DocuSign API. It includes:

* Authentication; 
* Create and send an envelop to DocuSign;
* Check envelop status;