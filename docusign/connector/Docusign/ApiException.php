<?php

namespace Docusign;

/**
 * Docusign specific exceptions. More could be added if needed.
 *
 * @author simon
 *
 */
class ApiException extends \Exception {

	const DOCUSIGN_AUTH_ERROR = 1;
	const DOCUSIGN_SEND_ERROR = 2;

}