<?php

namespace Docusign;

use \Docusign\Model\Auth as Auth;

require_once __DIR__ . '/autoload.php';

/**
 * This is a facade class for accessing Docusign models, and performing general
 * operations. It depends on php-curl to communicate to DocuSign services.
 *
 * @author simon
 *
 */
class API {
    const SERVICE_URL       = 'https://www.docusign.net/restapi/v2/login_information';
    const SERVICE_URL_DEMO  = 'https://demo.docusign.net/restapi/v2/login_information';

    protected $auth = null;

    public function __construct(Auth $auth) {
        $this->auth = $auth;
        
        if (is_null($this->auth->getAccountId())) {
            $this->auth->login();
        }
    }

    /**
     * Get the auth object of this API service instance
     *
     * @return Auth
     */
    public function getAuth() {
        return $this->auth;
    }

    /**
     * Wrapper function to send envelope to DocuSign.
     *
     * @param \Docusign\Model\Envelope $envelope
     * @return Array response array from DocuSign
     */
    public function sendEnvelope(\Docusign\Model\Envelope $envelope) {
        return $envelope->send($this->auth);
    }

    /**
     * Get DocuSign envelope information by a given id.
     * This is used to check
     * envelope status and other properties.
     * Return null if the communication failed, or bad request or other reason.
     * Check error_log for detail info.
     *
     * @param int $envelopeId
     * @return NULL | \Docusign\Model\Response\Envelope
     */
    public function getResponseEnvelope($envelopeId) {
        $curl = curl_init($this->auth->getBaseUrl() . "/envelopes/" . $envelopeId);
        
        if ($this->auth->isDemo) {
            curl_setopt($curl, CURLOPT_VERBOSE, true);
        }
        
        curl_setopt($curl, CURLOPT_CAINFO, DOCUSIGN_ROOT . '/cacert.pem');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array('X-DocuSign-Authentication: ' . $this->auth->getAuthenticationHeader()));
        
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 200) {
            error_log("error calling webservice in " . __NAMESPACE__ . ' | ' . __CLASS__ . ' | ' . __FUNCTION__ .
                        ", status is:" . $status . ". $json_response \n");
            
            throw new ApiException("Cannot get info of envelope '$envelopeId' from DocuSign by current user.");
        } else {
            $response = json_decode($json_response, true);
            
            return new \Docusign\Model\Response\Envelope($response);
        }
    }

    /**
     * Get DocuSign envelope recipients info by a given envelope id.
     * Recipient
     * info will tell his/her name, email, action on the envelope, timestamp
     * and some other info.
     * Return null if communication failed, bad request or maybe others. check
     * web server's error_log for detail info.
     *
     * @param int $envelopeId
     * @return NULL | \Docusign\Model\Response\Recipients
     */
    public function getResponseRecipients($envelopeId) {
        $curl = curl_init($this->auth->getBaseUrl() . "/envelopes/" . $envelopeId . "/recipients");
        
        if ($this->auth->isDemo) {
            curl_setopt($curl, CURLOPT_VERBOSE, true);
        }
        
        curl_setopt($curl, CURLOPT_CAINFO, DOCUSIGN_ROOT . '/cacert.pem');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array('X-DocuSign-Authentication: ' . $this->auth->getAuthenticationHeader()));
        
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 200) {
            error_log("error calling webservice in " . __NAMESPACE__ . ' | ' . __CLASS__ . ' | ' . __FUNCTION__ .
                        ", status is:" . $status . ". $json_response \n");
            
            throw
                new ApiException("Cannot get recipients info of envelope '$envelopeId' from DocuSign by current user.");
        } else {
            $response = json_decode($json_response, true);
            
            $recipients = new \Docusign\Model\Response\Recipients();
            foreach ( $response['signers'] as $signer ) {
                $recipients->addRecipient(new \Docusign\Model\Response\Recipient($signer));
            }
            return $recipients;
        }
    }

    /**
     * Send a request to DocuSign to void an envelope by given id.
     * DocuSign doesn't return anything on successful call, in case of error,
     * check error_log for detail.
     *
     * @param string $envelopeId
     *
     * @return boolean, on whether it's successful
     *
     */
    public function voidEnvelope($envelopeId) {
        $payload = json_encode(
            array(  'voidedReason'  => 'Following deletion of documents in MasterStream System',
                    'status'        => \Docusign\Model\Envelope::STATUS_VOIDED )
        );
        
        $curl = curl_init($this->auth->getBaseUrl() . "/envelopes/" . $envelopeId);
        
        if ($this->auth->isDemo) {
            curl_setopt($curl, CURLOPT_VERBOSE, true);
        }
        
        curl_setopt($curl, CURLOPT_CAINFO,          DOCUSIGN_ROOT . '/cacert.pem');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,  true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(  'Content-Type: application/json',
                    'Content-Length: ' . strlen($payload),
                    'X-DocuSign-Authentication: ' . $this->auth->getAuthenticationHeader() )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST,   "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS,      $payload);
        
        $json_response = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 200) {
            error_log("error calling webservice to 'void envelope' $envelopeId in " . __NAMESPACE__ .
                        ' | ' . __CLASS__ . ' | ' . __FUNCTION__ . ", status is:" . $status);
            return false;
        }

        return true;
    }

    /**
     * create an instance of Docusign\API class from given a ma company and a user (with DocuSign account associated).
     * Throws exception if the company has not setup integration with DocuSign.
     *
     * @param int $ma_company_id
     * @param int $user_id
     * @throws Exception
     * @return \Docusign\API
     */
    public static function getInstanceFromCompany($ma_company_id, $user_id) {
        if (!isset($ma_company_id)) {
            throw new ApiException('MA company id is required to look up its DocuSign API credentials', 1);
        }
        
        try {
            /* @var $macomp \Msts\MACompany */
            $macomp = \Msts\MACompany::find($ma_company_id);
        } catch ( \ActiveRecord\RecordNotFound $e ) {
            throw new ApiException("Can't seem to find the MA company, id:[$ma_company_id]", 1);
        }
        
        if (!$macomp->isFeatureEnabled(\Msts\MACompany::FEATURE_DOCUSIGN)) {
            throw new ApiException('Your DocuSign integration is disabled', 1);
        }
        
        $esign = $macomp->system_setup->esign;
        
        $eSignMode = $esign->esign_mode;
        $user_id = ('single' == $eSignMode) ? -1 : (int) $user_id;
        
        $esign_account = \Msts\MACompany\EsignCredential::first(array(
            'conditions' => array('active = \'t\' AND ma_company_id = ? AND agent_id = ?', $ma_company_id, $user_id)));
        
        if (!$esign_account) {
            throw new ApiException('Couldn\'t find Docusign integration credentials for company [' . $ma_company_id .
                    '], agent [' . $user_id . ']. Could it be inactive?', 1);
        }
        
        return new self(new Auth($esign_account->email, $esign_account->password, $esign_account->key_type));
    }
}