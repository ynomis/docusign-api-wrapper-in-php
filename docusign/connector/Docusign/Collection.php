<?php

namespace Docusign;

/**
 * Base class for object collections.
 *
 * @author simon
 *
 */
abstract class Collection implements \Iterator, \ArrayAccess, \Countable {

	/**
     * @var array
     */
    protected $_data;

    public function __construct() {
    	$this->_data = array();
    }

	/**
	 * Iterator methods
	 * @return mixed
	 */
	function rewind() {
	  return reset($this->_data);
	}
	function current() {
	  return current($this->_data);
	}
	function key() {
	  return key($this->_data);
	}
	function next() {
	  return next($this->_data);
	}
	function valid() {
	  return key($this->_data) !== null;
	}

    /**
     * ArrayAccess methods
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_data[] = $value;
        } else {
            $this->_data[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->_data[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->_data[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
    }

    /**
     * Countable method
     */
    public function count() {
        return count($this->_data);
    }

}