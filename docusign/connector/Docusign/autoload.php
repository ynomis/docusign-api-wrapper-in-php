<?php

/**
 * Require this file should autoload all classes in Docusign namespace.
 * And all those classes are lazy-loading in general.
 *
 */

/**
 * This is needed for setting cURL SSL cert.
 */
defined('DOCUSIGN_ROOT') or define('DOCUSIGN_ROOT', __DIR__);

spl_autoload_register(function($classname) {
	if(strpos($classname, 'Docusign') !== false) { // For now, only auto load Docusign classes
		$paths = explode('\\', $classname);
		array_shift($paths); // get rid of the root namespace part, because it's already included in __DIR__
		$filename = __DIR__ . '/' . implode(DIRECTORY_SEPARATOR, $paths) . '.php';
		if (file_exists($filename)) {
			include_once($filename);
		} else {
			error_log('Autoload class failed: ' . $filename);
		}
	}
});

