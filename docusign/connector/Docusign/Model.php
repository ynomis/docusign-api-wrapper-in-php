<?php

namespace Docusign;

/**
 * Base class for Docusign models. Having magic set and get functions here
 * to allow public access to private members. To provide access restriction,
 * or other controls over certain member, create set or get function around
 * the property. E.g.
 *     function setPassword($) {
 *         return null;
 *     }
 * will protect $class->password from being set publicly.
 *
 * @author simon
 *
 */
class Model {

	public function __get($name) {
		$get_func = 'get' . ucfirst($name);
		if (method_exists($this, $get_func)) {
			$this->$get_func($name);
		}elseif (isset($this->{$name}) || property_exists($this, $name)) {
			return $this->{$name};
		}
	}

	public function __set($name, $value) {
		$set_func = 'set' . ucfirst($name);
		if (method_exists($this, $set_func)) {
			$this->$set_func($value);
		} elseif (isset($this->{$name}) || property_exists($this, $name)) {
			$this->{$name} = $value;
		}
	}

	/**
	 * Convert the time in DocuSign response to a target timezone, most likely from the MA
	 * company setup. DocuSign uses UTC time in its responses.
	 *
	 * @param unknown $UTCTime, eg. 2013-04-25T00:09:39.7630000Z
	 * @param unknown $targetTimezone, php supported timezone, eg. America/Los_Angeles
	 * @param string $format, php DateTime supported time format, e.g. DateTime::RFC822, 'Y-m-d H:i:s'
	 */
	public static function getFormattedTime($UTCTime, $targetTimezone, $format = 'Y-m-d H:i:s T') {
		$date = new \DateTime($UTCTime, new \DateTimeZone('UTC'));

		if (!is_null($targetTimezone)) {
			try {
				$date->setTimezone(new \DateTimeZone($targetTimezone));
			} catch (\Exception $e) {
				error_log($e->getMessage());
			}
		}

		return $date->format($format);
	}

// 	abstract function toArray();

}