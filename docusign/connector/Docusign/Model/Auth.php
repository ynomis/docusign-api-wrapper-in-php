<?php

namespace Docusign\Model;

use \Docusign\ApiException as ApiException;
use \Docusign\API as API;

/**
 *
 *
 * DocuSign REST request consists of 2 calls. The first one is authentication.
 * This is representing the authentication. Once it's successful, it will
 * contain accountId, baseUrl from the service.
 *
 * @author simon
 *
 */
class Auth extends \Docusign\Model {
    
    /**
     * DocuSign uses only one integratorKey for API calls from different accounts, as long
     * as they are using the same piece of code. And it could be the very same one from
     * sandbox to production. Even multiple customers of us are going to go through this
     * very same key. And each new key has to be verified before taking effect in their
     * production enviornment.
     *
     * @var string
     */
    const DOCUSIGN_INTEGRATOR_KEY = '/* DocuSign API key here */';

    public $isDemo = false;

    protected $email                = null;
    protected $password             = null;
    protected $integratorKey        = null;
    protected $endpointType         = null;
    protected $name                 = null;
    protected $sendOnBehalfOfEmail  = null;
    protected $sendOnBehalfOfName   = null;
    
    // below are returned values from Docusign
    protected $accountId    = null;
    protected $baseUrl      = null;
    protected $userId       = null;
    protected $userName     = null;
    
    // It's a hack, to force my sandbox account into debug mode, -syang
    private static $forceDemoAccounts = array();

    /**
     * Construct an api service instance.
     * Parameters could be ideally reaplced with
     * a persisted AR object
     *
     * @param string $email
     * @param string $password
     * @param string $endpointType
     * @param string $name
     * @throws ApiException
     */
    public function __construct($email, $password, $endpointType = 'production', $name = null) {
        if (!isset($email) || $email == null) {
            throw new ApiException('Email is required to construct a DocuSign_Service instance.',
                ApiException::DOCUSIGN_AUTH_ERROR);
        }
        if (!isset($password) || $password == null) {
            throw new ApiException('Password is required to construct a DocuSign_Service instance.',
                ApiException::DOCUSIGN_AUTH_ERROR);
        }

        $this->email            = $email;
        $this->password         = $password;
        $this->integratorKey    = self::DOCUSIGN_INTEGRATOR_KEY;
        $this->endpointType     = $endpointType;
        $this->name             = $name;
    }

    /**
     * Do the actual call to authenticate.
     *
     * @throws ApiException, when communication is failed or request is bad
     */
    public function login() {
        if ($this->endpointType != 'production') {
            $this->isDemo = true;
        }
        
        // @see above comment on self::$forceDemoAccounts
        if (!$this->isDemo && in_array($this->email, self::$forceDemoAccounts)) {
            $this->isDemo = true;
        }
        
        $service_url = ($this->isDemo) ? API::SERVICE_URL_DEMO : API::SERVICE_URL;
        
        $curl = curl_init($service_url);
        
        if ($this->isDemo) {
            curl_setopt($curl, CURLOPT_VERBOSE, true);
        }
        
        curl_setopt($curl, CURLOPT_CAINFO, DOCUSIGN_ROOT . '/cacert.pem');
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: " . $this->getAuthenticationHeader()));
        
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        if ($status != 200) {
            error_Log("error calling docusign webservice, status is: " . $status . ". $json_response\n");
            throw new ApiException("API login failed, error calling docusign webservice, status is: " . $status, 1);
        }
        
        $response = json_decode($json_response, true);
        
        $accountId  = $response["loginAccounts"][0]["accountId"];
        $baseUrl    = $response["loginAccounts"][0]["baseUrl"];
        
        $this->accountId    = $response["loginAccounts"][0]["accountId"];
        $this->baseUrl      = $response["loginAccounts"][0]["baseUrl"];
        $this->userId       = $response["loginAccounts"][0]["userId"];
        $this->userName     = $response["loginAccounts"][0]["userName"];
        
        curl_close($curl);
    }

    /**
     * Helper function to compose authentication header in 2nd request, which
     * is usally the call to get actual data.
     *
     * @param bool $includeSendOnBehalfOf, true to include sendOnBelafOf person in header
     * @return string
     */
    public function getAuthenticationHeader($includeSendOnBehalfOf = false) {
        $_sender = '';
        if ($includeSendOnBehalfOf &&
            !is_null($this->sendOnBehalfOfEmail) &&
            strlen($this->sendOnBehalfOfEmail) > 0) {
            $_name      = (is_null($this->sendOnBehalfOfName)) ? '' : ':' . $this->sendOnBehalfOfName;
            $_sender    = "<SendOnBehalfOf>{$this->sendOnBehalfOfEmail}{$_name}</SendOnBehalfOf>";
        }
        
        $header = $_sender . "<DocuSignCredentials><Username>" . $this->email . "</Username><Password>" .
            $this->password . "</Password><IntegratorKey>" .
            $this->integratorKey . "</IntegratorKey></DocuSignCredentials>";
        
        return $header;
    }

    /**
     * To conceal password from external access.
     *
     * @return NULL
     */
    public function getPassword() {
        throw new \Exception('Password property of class ' . __CLASS__ . ' can only be accessed internally.');
    }

    /**
     * I do have easy access to any memebers, this is just a wrapper.
     */
    public function getAccountId() {
        return $this->accountId;
    }

    /**
     * Wrapper
     */
    public function getBaseUrl() {
        return $this->baseUrl;
    }

    /**
     * Return the state of whether the authentication is done and successful
     */
    public function isLoggedIn() {
        return null != $this->getAccountId() && strlen($this->getAccountId()) > 0;
    }

}