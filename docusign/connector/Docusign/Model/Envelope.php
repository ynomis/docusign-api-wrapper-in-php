<?php

namespace Docusign\Model;

use Docusign\ApiException;

use \Docusign\Model\Recipient as Recipient;
use \Docusign\Model\Document as Document;

/**
 * Wrapper for DocuSign request envelope object.
 *
 * @author simon
 *
 */
class Envelope extends \Docusign\Model {

    const SIGNING_LOCATION_ONLINE    = 'Online';
    const SIGNING_LOCATION_IN_PERSON = 'InPerson';

    const STATUS_SENT                = 'sent';
    const STATUS_CREATED             = 'created';
    const STATUS_ANY                 = 'any';
    const STATUS_VOIDED              = 'voided';
    const STATUS_DELETED             = 'deleted';
    const STATUS_DELIVERED           = 'delivered';
    const STATUS_SIGNED              = 'signed';
    const STATUS_COMPLETED           = 'completed';
    const STATUS_DECLINED            = 'declined';
    const STATUS_TIMEDOUT            = 'timedout';
    const STATUS_TEMPLATE            = 'template';
    const STATUS_PROCESSING          = 'processing';

    const STATUS_INTERNAL_DELETED	 = 'internally deleted';

//    protected $envelopeId;
//    protected $createdDateTime;
//    protected $sentDateTime;


    /**
     * The subject of the email that will be sent to the recipient. This can be
     * a maximum of 100 characters.
     *
     * @var string
     */
    protected $subject;


    /**
     * The email body for the email to all recipients.  Max 2000 characters.
     *
     * @var string
     */
    protected $blurb;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var array
     */
    protected $customFields;

    /**
     * @var array
     */
    protected $documents = array();

    /**
     * @var array
     */
    protected $recipients = array();

    /**
     * @var string
     */
    protected $signingLocation = self::SIGNING_LOCATION_ONLINE;

    /**
     * Allow the recipient to redirect the envelope to another recipient.
     *
     * @var bool
     */
    protected $allowReassign = false;

    /**
     * Allow recipients to appear in more than one place in the routing order.
     *
     * @var bool
     */
    protected $allowRecipientRecursion = false;

    /**
     * Enable Authoritative Copy feature.
     *
     * @var bool
     */
    protected $authoritativeCopy = false;

    /**
     * Require the signer is required to have a signature or initial tab on the
     * document.  Account required "Document Visibility" enabled for this
     * feature.
     *
     * @var boolean
     */
    protected $enforceSignerVisibility = false;

    /**
     * Allow signer to print the document and sign it.
     *
     * @var boolean
     */
    protected $enableWetSign = false;


    /**
     * Used to segment different part in envelope body
     *
     * @var string
     */
    protected $boundary = null;

    /**
     * Set send status of this envelope
     * It's a wrapper served as kinda alias, 'cause base model has magic
     * function for general accessor
     *
     * @param string $status
     */
    public function setStatus($status) {
    	$this->status = $status;
    }

    /**
     * @param string $subject
     *   Maximum of 100 characters
     *
     * @return Envelope
     */
    public function setSubject($subject) {
        $this->subject = strlen($subject) > 100 ? substr($subject, 0, 100) : $subject;
        return $this;
    }

	/**
	 * Not all properties are used, yet tested. May add when needed.
	 *
	 * @return array
	 */
    public function toArray() {
        $serialized = array(
//          'allowReassign'           => $this->allowReassign,
//          'allowRecipientRecursion' => $this->allowRecipientRecursion,
//          'authoritativeCopy'       => $this->authoritativeCopy,
            'emailBlurb'              => $this->blurb,
            'emailSubject'            => $this->subject,
//          'enableWetSign'           => $this->enableWetSign,
//          'enforceSignerVisibility' => $this->enforceSignerVisibility,
//          'signingLocation'         => $this->signingLocation,
            'status'                  => $this->status,
        );

        if (count($this->customFields)) {
            foreach ($this->customFields as $customField) {
                $serialized['customFields'][] = $customField->toArray();
            }
        }

        if (count($this->documents)) {
            foreach ($this->documents as $document) {
                $serialized['documents'][] = $document->toArray();
            }
        }

        if (count($this->recipients)) {
            foreach ($this->recipients as $recipient) { /* @var $recipient Recipient */
                $serialized['recipients'][$recipient->getType()][] = $recipient->toArray();
            }
        }

        return $serialized;
    }

    /**
     * Add a recipient to the envelope
     *
     * @param Recipient $recipient
     */
    public function addRecipient(Recipient $recipient) {
		$this->recipients[] = $recipient;
    }

    /**
     * Add a document to the envelope
     *
     * @param Document $document
     * @throws ApiException
     */
    public function addDocument(Document $document) {
    	if (file_exists($document->path)) {
	    	$this->documents[] = $document;
    	} else {
    		throw new ApiException('Error when adding document to envelope, file does not exist: ' . $document->path);
    	}
    }

    /**
     * Compose the MIME body for DocuSign request through CURL call.
     *
     * @return string
     */
    public function getMimeBody() {
    	$this->boundary = md5(date('r', time()));
    	$data_string = json_encode($this->toArray());

    	$document_contents = '';
    	foreach ($this->documents as $doc) { /* @var $doc Document */
    		$file_contents = file_get_contents($doc->path);
    		$document_contents .=  "--{$this->boundary}\r\n"
				."Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n"
				."Content-Disposition: file; filename=\"{$doc->name}\"; documentid={$doc->documentId} \r\n"
				."\r\n"
				."$file_contents\r\n";
    	}

		$mime_string = "\r\n"
			."\r\n"
			."--{$this->boundary}\r\n"
			."Content-Type: application/json\r\n"
			."Content-Disposition: form-data\r\n"
			."\r\n"
			."$data_string\r\n"
			.$document_contents
			."--{$this->boundary}--\r\n"
			."\r\n";

		return $mime_string;
    }

    /**
     *
     * Send a signing request through DocuSign.
     * Note, the response here is not the object of Docusign\Model\Response\Envelope.
     * I don't bother to create a dedicate wrapper here, so just use straight php
     * array for now.
     *
	 * response from sending doc to DocuSign
	 * --------------------------------------------------------
	 * "envelopeId": "8daa300f-84cf-4fab-9fd3-003342d03ff2",
	 * "status": "sent",
	 * "statusDateTime": "2013-03-04T19:12:56.2530000Z",
	 * "uri": "/envelopes/8daa300f-84cf-4fab-9fd3-003342d03ff2"
     *
     * @param \Docusign\Model\Auth $auth
     * @return Array
     * @throws \Docusign\ApiException
     *
     */
    public function send(\Docusign\Model\Auth $auth) {
    	if (!count($this->recipients)) {
    		throw new \Docusign\ApiException('No recipient for this envelope.');
    	}

    	// Assume we only do DocuSign request on documents
    	if (!count($this->documents)) {
    		throw new \Docusign\ApiException('No document attached for this envelope.');
    	}

    	if (!$auth->isLoggedIn()) {
    		$auth->login();
    	}

	    $curl = curl_init($auth->getBaseUrl() . "/envelopes" );

    	if ($auth->isDemo) {
			curl_setopt($curl, CURLOPT_VERBOSE, true);
	    }

	    $body = $this->getMimeBody();
	    curl_setopt($curl, CURLOPT_CAINFO, DOCUSIGN_ROOT . '/cacert.pem');
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
	    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	        'Content-Type: multipart/form-data;boundary=' . $this->boundary,
	        'Content-Length: ' . strlen($body),
	        'X-DocuSign-Authentication: ' . $auth->getAuthenticationHeader() )
	    );

	    $json_response = curl_exec($curl);
	    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    if ( $status != 201 ) {
	        error_log("error calling webservice, status is:" . $status . "\n");
	        error_log("error $json_response");

	        return null;
	    } else {
		    $response = json_decode($json_response, true);

		    // $envelopeId = $response["envelopeId"];
		    // echo "Document is sent! Envelope ID: " . $envelopeId;
		    return $response;
	    }
    }

}