<?php

namespace Docusign\Model\AnchorTab;

/**
 * Automatic Tabs used in document send to signer
 * For 'Tab Parameters' visit here for reference:
 * http://www.docusign.com/p/RESTAPIGuide/RESTAPIGuide.htm#REST%20API%20References/Tab Parameters.htm
 *
 * @author simon
 *
 */
class InitialHere extends \Docusign\Model\AnchorTab {

    protected $tabType = parent::TYPE_INITIAL_HERE;

    protected function setAttributes() {
    	$this->attributes = array(
			'anchorString'            	=> null,
			'anchorXOffset'           	=> null,
			'anchorYOffset'           	=> null,
			'anchorIgnoreIfNotPresent'	=> true,
			'anchorUnits'             	=> null,
			'conditionalParentLabel'  	=> null,
			'conditionalParentValue'  	=> null,
			'documentId'              	=> null,
			'pageNumber'              	=> null,
			'recipientId'             	=> null,
			'templateLocked'          	=> null,
			'templateRequired'        	=> null,
			'xPosition'               	=> null,
			'yPosition'               	=> null,
			'name'                    	=> null,
			'optional'                	=> null,
			'scaleValue'              	=> null,
			'tabLabel'                	=> null,
        );
    }

}