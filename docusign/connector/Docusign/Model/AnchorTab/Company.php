<?php

namespace Docusign\Model\AnchorTab;

/**
 * Automatic Tabs used in document send to signer
 * For 'Tab Parameters' visit here for reference:
 * http://www.docusign.com/p/RESTAPIGuide/RESTAPIGuide.htm#REST%20API%20References/Tab Parameters.htm
 *
 * @author simon
 *
 */
class Company extends \Docusign\Model\AnchorTab {

    protected $tabType = parent::TYPE_COMPANY;

    protected function setAttributes() {
    	$this->attributes = array(
            'anchorString'                => null,
            'anchorXOffset'               => null,
            'anchorYOffset'               => null,
            'anchorIgnoreIfNotPresent'    => true,
            'anchorUnits'                 => null,
            'conditionalParentLabel'      => null,
            'conditionalParentValue'      => null,
            'documentId'                  => null,
            'pageNumber'                  => null,
            'recipientId'                 => null,
            'templateLocked'              => null,
            'templateRequired'            => null,
            'xPosition'                   => null,
            'yPosition'                   => null,
            'bold'                        => null,
            'font'                        => null,
            'fontColor'                   => null,
            'fontSize'                    => null,
            'italic'                      => null,
            'tabLabel'                    => null,
            'underline'                   => null,
            'concealValueOnDocument'      => null,
            'disableAutoSize'             => null,
            'locked'                      => null,
            'name'                        => null,
            'required'                    => null,
            'value'                       => null,
            'width'                       => null,
    	);
    }

}