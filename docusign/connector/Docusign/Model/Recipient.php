<?php

namespace Docusign\Model;

/**
 * Recipient in DocuSign request.
 * $type is set here to differenciate sender
 * and receiver. But in current case, only 'signers', the receiver, is used
 * in making document request.
 *
 * @author simon
 *
 */
class Recipient extends \Docusign\Model {
    const TYPE_ORIGINATOR = 'senders'; // Whoever creates the document, and requesting the signing process
                                       // This is actaully not supported by Docusign API
    const TYPE_RECIPIENT = 'signers'; // Whoever receives the document, and completes the signing process

    protected $type; // originator, or recipient
    protected $email;
    protected $name;
    protected $identifier; // Recipient identifier
    protected $routingOrder;

    protected $tabs = array();

    /**
     * I leave $name and $identifier optional here, but it appears DocuSign
     * requires those fields with values. Although I'm not enforing here,
     * it should be contructed with values. Otherwise, you may get bad
     * request error from DocuSIgn service.
     *
     *
     * @param string $type
     * @param string $email
     * @param string $name
     * @param string $identifier
     * @param string $routingOrder
     */
    public function __construct($type, $email, $name = null, $identifier = null, $routingOrder = null) {
        if (self::TYPE_ORIGINATOR != $type || self::TYPE_RECIPIENT != $type) {
            $this->type = self::TYPE_RECIPIENT;
        } else {
            $this->type = $type;
        }
        
        $this->email        = $email;
        $this->name         = $name;
        $this->identifier   = $identifier;
        $this->routingOrder = $routingOrder;
    }

    /**
     * Accessor
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Flatten the object, for easy composing of CURL request body.
     *
     * @return array
     */
    public function toArray() {
        $tabs = array();
        foreach ( $this->tabs as $tab ) { /* @var $tab \Docusign\Model\AnchorTab */
            $type = $tab->getTabType() . 'Tabs';
            
            $tabs[$type][] = $tab->toArray();
        }
        
        return array(   'email'         => $this->email,
                        'name'          => $this->name,
                        'recipientId'   => $this->identifier,
                        'routingOrder'  => $this->routingOrder,
                        'tabs'          => $tabs );
    }

    /**
     * Add a tab to this recipient
     */
    public function addTab(AnchorTab $tab) {
        $this->tabs[] = $tab;
    }

    /**
     * This is only an implementation based on current agreement of using auto anchors.
     * Should it be changed, this function needs to be revised/overriden.
     *
     * Add all auto anchor tabs, format:
     * "%%{$anchor_string}[{$anchor_sequence}][::{$signer_suffix}]%%"
     * If there is only one anchor of a type, $anchor_sequence could be ignored, for backward compatibility
     * If there is only one signer, $signer_suffix coulb be ignored, for backward compatibility
     *
     * @param $suffix, suffix
     *            in anchor placeholder to denote each signers
     *
     */
    public function addAllTabs($suffix = null) {
        $sp = '::';
        $suffix = (is_null($suffix) && $this->identifier > 1) ? $this->identifier : $suffix;
        $suffix = (!is_null($suffix)) ? $sp . $suffix : '';
        
        foreach ( array(AnchorTab::TYPE_SIGN_HERE,
                        AnchorTab::TYPE_INITIAL_HERE,
                        AnchorTab::TYPE_COMPANY,
                        AnchorTab::TYPE_EMAIL,
                        AnchorTab::TYPE_FULL_NAME,
                        AnchorTab::TYPE_TEXT,
                        AnchorTab::TYPE_TITLE,
                        AnchorTab::TYPE_DATE_SIGNED,
                        AnchorTab::TYPE_DATE,
                        AnchorTab::TYPE_NUMBER) as $anchor_string ) {
            $this->addTab(AnchorTab::getAnchorTab($anchor_string, "%%{$anchor_string}{$suffix}%%"));
        }
        
        // Following tabs could have 5 more instances
        foreach ( array(AnchorTab::TYPE_TEXT,
                        AnchorTab::TYPE_DATE,
                        AnchorTab::TYPE_NUMBER) as $anchor_string ) {
            for($i = 1; $i <= 5; $i++) {
                $this->addTab(AnchorTab::getAnchorTab($anchor_string, "%%{$anchor_string}{$i}{$suffix}%%"));
            }
        }
    }

}