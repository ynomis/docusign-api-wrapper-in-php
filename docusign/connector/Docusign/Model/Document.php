<?php

namespace Docusign\Model;

/**
 *
 *
 * Document object in DocuSign request. It actually represents a concrete file
 * on file system, but since the file is generated on the fly, it may not make
 * sense to validate path here. But to make successful request, you should add
 * file_exist() check whenever adding it to the request.
 *
 * @see Docusign\Model\Envelope->addDocument()
 *
 * @author simon
 *
 */
class Document extends \Docusign\Model {

    protected $documentId;
    protected $name;
    protected $path;
    protected $order;
    protected $transformPdfFields;

    public function __construct($identifier, $path, $name = null) {
        $this->documentId   = $identifier;
        $this->path         = $path;
        if (is_null($name)) {
            $this->name = basename($path);
        } else {
            $this->name = $name;
        }
    }

    /**
     * Flatten the object for easy composing DocuSign request body
     *
     * @return array
     */
    public function toArray() {
        return array('documentId' => $this->documentId, 'name' => $this->name);
    }

}