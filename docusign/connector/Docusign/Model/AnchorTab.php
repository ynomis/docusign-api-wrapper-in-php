<?php

namespace Docusign\Model;

/**
 * Automatic Tabs used in document send to signer
 * For 'Tab Parameters' visit here for reference:
 * http://www.docusign.com/p/RESTAPIGuide/RESTAPIGuide.htm#REST%20API%20References/Tab Parameters.htm
 * 
 * @author simon
 *
 */
abstract class AnchorTab extends \Docusign\Model {
	CONST TYPE_SIGN_HERE	= 'signHere';
	CONST TYPE_INITIAL_HERE	= 'initialHere';
	CONST TYPE_FULL_NAME	= 'fullName';
	CONST TYPE_EMAIL		= 'email';
	CONST TYPE_TEXT			= 'text';
	CONST TYPE_TITLE		= 'title';
	CONST TYPE_COMPANY		= 'company';
	CONST TYPE_DATE_SIGNED	= 'dateSigned';         // Automatic date
	CONST TYPE_DATE			= 'date';               // User input date
	CONST TYPE_NUMBER     	= 'number';

	// Following may not work with DocuSign yet.
	CONST TYPE_APPROVE    	= 'approve';
	CONST TYPE_ZIP        	= 'zip';
	CONST TYPE_SSN        	= 'ssn';

	protected $tabType;

	/**
	 * Anchor Tab attributes supported by DocuSign. Set by individual sub classes.
	 * 
	 * @var array
	 */
	protected $attributes = array();

	/**
	 * Set DocuSign acceptable attributes for the specific anchor tab
	 * 
	 * @param array $attributes
	 */
	abstract protected function setAttributes();

	/**
	 * Get a concrete Tab class based on tabType
	 * 
	 * @param string $tabType
	 * @param string $anchorString
	 * @param string $label
	 * 
	 * @throws \Exception, if specified tabType is not defined
	 */
	public static function getAnchorTab($tabType, $anchorString, $label = null) {
	    $klass = new \ReflectionClass(get_called_class());
        $constants = array();
	    foreach ($klass->getConstants() as $constKey => $constVar) {
	    	if (0 === strpos($constKey, 'TYPE_')) {
	    		$constants[$constKey] = $constVar;
	    	}
	    }

	    if (in_array($tabType, $constants)) {
    	    $tabClass = '\\Docusign\\Model\\AnchorTab\\' . ucfirst($tabType);
	    	return new $tabClass($anchorString, $label);
	    } else {
            throw new \Exception("Cannot instantiate DocuSign Anchor Tab of '{$tabType}'");
	    }
	}

	public function __construct($anchorString, $label = null) {
	    $this->setAttributes();
		$this->attributes['anchorString']		= $anchorString;
		$this->attributes['tabLabel']			= $label ?: $anchorString;
	}

	public function __set($name, $value) {
		if (array_key_exists($name, $this->attributes)) {
			$this->attributes[$name] = $value;
		} else {
    		$trace = debug_backtrace();
    		trigger_error(
                'Unacceptable property via __set: ' . $name . ' in ' . $trace[0]['file'] . ' on line ' . $trace[0]['line'],
    		E_USER_NOTICE);
		}
	}

	public function __get($name) {
		if (array_key_exists($name, $this->attributes)) {
			return $this->attributes[$name];
		}

		$trace = debug_backtrace();
		trigger_error(
            'Unacceptable property via __get: ' . $name . ' in ' . $trace[0]['file'] . ' on line ' . $trace[0]['line'],
		E_USER_NOTICE);

		return null;
	}

	public function getTabType() {
		return $this->tabType;
	}

	public function toArray() {
		return array(
			'anchorString'	=> $this->anchorString,
			'tabLabel'		=> $this->tabLabel,
			'anchorIgnoreIfNotPresent'	=> $this->anchorIgnoreIfNotPresent,
		);
	}

}