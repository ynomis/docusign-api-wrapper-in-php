<?php

namespace Docusign\Model\Response;

/**
 *
 * Class wrapper of DocuSign envelope info in response. Note, it's different
 * from the one in request. They have different set of members.
 *
 * @author simon
 *
 */
class Envelope extends \Docusign\Model {

	protected $allowReassign;
	protected $certificateUri;
	protected $createdDateTime;
	protected $customFieldsUri;
	protected $documentsCombinedUri;
	protected $documentsUri;
	protected $emailBlurb;
	protected $emailSubject;
	protected $enableWetSign;
	protected $envelopeId;
	protected $envelopeUri;
	protected $notificationUri;
	protected $recipientsUri;
	protected $sentDateTime;
	protected $status;
	protected $statusChangedDateTime;
	protected $templatesUri;

	/**
	 * Above properties needs to be updated if DocuSign spec is changed.
	 *
	 * @param array $response
	 */
	public function __construct($response) {
		foreach ($response as $k => $v) {
			$this->{$k} = $v;
		}
	}

	/**
	 *
	 * Convert object to php array
	 *
	 * @return array
	 */
	public function toArray($targetTimezone = null) {
		return array(
			'allowReassign'         => $this->allowReassign,
			'certificateUri'        => $this->certificateUri,
			'createdDateTime'       =>
				(!empty($this->createdDateTime)) ?
					static::getFormattedTime($this->createdDateTime, $targetTimezone) : null,
			'customFieldsUri'       => $this->customFieldsUri,
			'documentsCombinedUri'  => $this->documentsCombinedUri,
			'documentsUri'          => $this->documentsUri,
			'emailBlurb'            => $this->emailBlurb,
			'emailSubject'          => $this->emailSubject,
			'enableWetSign'         => $this->enableWetSign,
			'envelopeId'            => $this->envelopeId,
			'envelopeUri'           => $this->envelopeUri,
			'notificationUri'       => $this->notificationUri,
			'recipientsUri'         => $this->recipientsUri,
			'sentDateTime'          =>
				(!empty($this->sentDateTime)) ?
					static::getFormattedTime($this->sentDateTime, $targetTimezone) : null,
			'status'                => $this->status,
			'statusChangedDateTime' =>
				(!empty($this->statusChangedDateTime)) ?
					static::getFormattedTime($this->statusChangedDateTime, $targetTimezone) : null,
			'templatesUri'          => $this->templatesUri,
		);
	}

}