<?php

namespace Docusign\Model\Response;

/**
 *
 * Represents collection of recipient object in DocuSign response.
 *
 * @author simon
 *
 */
class Recipients extends \Docusign\Collection {

	/**
	 * Add a response recipient object to this collection
	 *
	 * @param \Docusign\Model\Response\Recipient $recipient
	 */
	public function addRecipient(\Docusign\Model\Response\Recipient $recipient) {
		$this->offsetSet($recipient->email, $recipient);
	}

	/**
	 * Remove a response object from this collection
	 *
	 * @param \Docusign\Model\Response\Recipient $recipient
	 */
	public function removeRecipient(\Docusign\Model\Response\Recipient $recipient) {
		$this->offsetUnset($recipient->email);
	}

	/**
	 * Convert this object to an array
	 *
	 * @return array
	 */
	public function toArray($targetTimezone = null) {
		$_temp = array();
		foreach($this->_data as $r) {
			$_temp[] = $r->toArray($targetTimezone);
		}

		return $_temp;
	}
}