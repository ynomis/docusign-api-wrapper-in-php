<?php

namespace Docusign\Model\Response;

/**
 *
 * Class wrapper of DocuSign recipient info in response. Note, it's different
 * from the one in request. They have different set of members.
 *
 * @author simon
 *
 */
class Recipient extends \Docusign\Model {

	protected $deliveredDateTime;
	protected $recipientId;
	protected $requireIdLookup;
	protected $routingOrder;
	protected $signedDateTime;
	protected $status;
	protected $email;
	protected $name;

	/**
	 * In case DocuSign changed spec on response, above properties needs update.
	 * Throw exception if email is not returned in response (in case of error).
	 *
	 * @param array $response
	 * @throws \Docusign\ApiException
	 */
	public function __construct($response) {
		if (!isset($response['email']) || !strlen($response['email'])) {
			throw new \Docusign\ApiException(
					'Recipient from response has no email address, which doesn\'t make sense.');
		}

		foreach ($response as $k => $v) {
			$this->{$k} = $v;
		}
	}

	/**
	 * Convert object to php array
	 *
	 * @return array
	 */
	public function toArray($targetTimezone = null) {
		return array(
			'deliveredDateTime' =>
				(!empty($this->deliveredDateTime)) ?
					static::getFormattedTime($this->deliveredDateTime, $targetTimezone) : null,
			'recipientId'       => $this->recipientId,
			'requireIdLookup'   => $this->requireIdLookup,
			'routingOrder'      => $this->routingOrder,
			'signedDateTime'    =>
				(!empty($this->signedDateTime)) ?
					static::getFormattedTime($this->signedDateTime, $targetTimezone) : null,
			'status'            => $this->status,
			'email'             => $this->email,
			'name'              => $this->name,
		);
	}
}