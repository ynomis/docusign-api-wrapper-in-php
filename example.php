<?php

    /**
     * Following are code snippet from live system.
     */
    require_once DOC_ROOT . "/connector/Docusign/autoload.php";

    $response = null;
	try {
		$api = \Docusign\API::getInstanceFromCompany($ma_company_id, $agent_id);

	    // add send-on-behalf-person here
	    // $auth = $api->getAuth();
	    // $auth->sendOnBehalfOfEmail	= '';
	    // $auth->sendOnBehalfOfName	= '';

		$envelope = new Docusign\Model\Envelope();
		$envelope->subject	= $subject;
		$envelope->blurb	= $body;
		$envelope->status	= Docusign\Model\Envelope::STATUS_SENT;

		require_once 'Mail/RFC822.php';
		require_once 'PEAR.php';
		$rcps = Mail_RFC822::parseAddressList($to);
		if ( PEAR::isError($rcps) ) {
			$_msg = sprintf( 'The given address string option "%s" is not RFC822 compliant.', htmlentities($to) );
			error_log($_msg);
			throw new \Docusign\ApiException($_msg);
		} else {
			foreach ($rcps as $index => $entry) {
				$_email = $entry->mailbox . '@' . $entry->host;
				$_name	= trim($entry->personal, ' "');
				$_name	= (strlen($_name) > 0) ? $_name : $_email;

				// the name part seeems required from DocuSign. In any case it's empty, use email address.
				$recipient = new Docusign\Model\Recipient(
					Docusign\Model\Recipient::TYPE_RECIPIENT, $_email, $_name, $index + 1, $index + 1);

				$recipient->addAllTabs();

				$envelope->addRecipient($recipient);
			}

			// Now adding second (last) signer
			if ($esign_setup->second_signer_enabled) { /* @var $esign_setup \Msts\MACompany\EsignSetup */
			    $lastsigner = new Docusign\Model\Recipient(
			            Docusign\Model\Recipient::TYPE_RECIPIENT,
			            $esign_setup->second_signer_email,
			            $esign_setup->second_signer_name,
			            $index + 2, $index + 2);
			    $lastsigner->addAllTabs('last'); // second signer use 'last' as suffix
			    $envelope->addRecipient($lastsigner);
			}
		}

		// Add document
		$orderDoc = array(/* Documents attached to this envelop */);
		for ($i = 0; $i < count($orderDoc); $i++)
		{

			$path = ''; // path to the file to send via DocuSign
			$doc = new Docusign\Model\Document($i + 1, $path);
			$envelope->addDocument($doc);

		}

		// send to DocuSign
		$response = $api->sendEnvelope($envelope);

	} catch (Docusign\ApiException $e) {
	    error_log($e->getMessage());
		$msg = $e->getMessage();
	}
if (!is_null($response)) {
    // write return message to db for referencing
    $envelope_info = array(
        'quote_sq_id'   => $quoteSqId,
        'order_id'      => ($_order->rowCount > 0) ? $_order->id : 'NULL',
        'envelope_id'   => $response['envelopeId'],
        'status'        => $response['status'],
        'agent_id'      => $agent_id
    );
    // ...
} else {
    // Error out
}

?>